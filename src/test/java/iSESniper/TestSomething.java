package iSESniper;

import org.junit.Test;

import iSESniper.cooperation.commit.enumeration.Type;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.TestSomething.java
 *  
 *  2018年5月30日	下午3:20:06  
*/

public class TestSomething {
	@Test
	public void testTypesEquals() {
		Type type = Type.valueOf("ADD");
		System.out.println(type == Type.ADD);
	}
}
