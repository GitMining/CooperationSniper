package iSESniper.cooperation.commit.extractor;

import java.io.IOException;
import java.util.List;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.junit.Test;

import iSESniper.cooperation.entity.Commit;
import iSESniper.cooperation.entity.FileLifeCircle;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.cooperation.commit.extractor.TestFileLfeCircleExtractor.java
 *  
 *  2018年4月25日	下午2:30:39  
*/

public class TestFileLfeCircleExtractor {

	private FileLifeCircleExtractor fileLifeCircleExtractor;
	
	private CommitsExtractor commitsExtractor;
	
	@Test
	public void testExtractFileLifeCircle() throws NoHeadException, IOException, GitAPIException {
		fileLifeCircleExtractor = new FileLifeCircleExtractor("");
		commitsExtractor = new CommitsExtractor("");
		List<Commit> commits = commitsExtractor.getCommits();
		int num = fileLifeCircleExtractor.extractFileLifeCircle(commits, null);
		for(FileLifeCircle f: fileLifeCircleExtractor.getFileLifeCircles()) 
			System.out.println(f);	
		System.out.println(num);
	}
}
