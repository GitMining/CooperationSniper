package iSESniper.cooperation.commit.extractor;

import java.io.IOException;
import java.util.List;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.junit.Test;

import iSESniper.cooperation.entity.Commit;
import iSESniper.cooperation.entity.FileDiff;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.cooperation.commit.extractor.CommitExtractorTest.java
 *  
 *  2018年4月17日	下午3:46:20  
*/

public class CommitExtractorTest {
	
	CommitsExtractor commitExtractor;

	String repo = "D:\\work\\GitMining\\Repos\\samples\\161250004_Joker\\Project_Phase_I";
	
	@Test
	public void testExtractProjectCommits() {
		commitExtractor = new CommitsExtractor(repo);
		try {
			commitExtractor.extractProjectCommits();
			List<Commit> commits = commitExtractor.getCommits();
			for(Commit c: commits) 
				if(c.getCommitId().equals("19234e4039d3158aad3fe7feddf8a086ab1bcf43")) {
					System.out.println(c.getCommitId());
					for(FileDiff fd: c.getFiles())
						System.out.println(fd);
					System.out.println(c.getFiles().size());
					System.out.println(c.getParents().get(0));
				}
//			System.out.println(commits);
		} catch (IOException | GitAPIException e) {
			e.printStackTrace();
		}
	}
}
