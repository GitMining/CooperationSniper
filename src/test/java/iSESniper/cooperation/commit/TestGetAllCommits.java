package iSESniper.cooperation.commit;

import java.io.IOException;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Repository;
import org.junit.Test;

import iSESniper.cooperation.commit.extractor.PrjtCommits;
import iSESniper.cooperation.entity.Commit;
import iSESniper.cooperation.entity.FileCommits;
import iSESniper.cooperation.helper.CooperationHelper;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.cooperation.commit.TestGetAllCommits.java
 *  
 *  2018年3月5日	下午5:58:32  
*/

public class TestGetAllCommits {

	private PrjtCommits gpac;
	
	@Test
	public void testGetAllCommits() {
		String pth = "D:/work/GitMining/Repos/TechTest";
//		String pth1= "D:\\work\\GitMining\\Repos\\test";
		String pth2= "";
		try {
			gpac = new PrjtCommits(pth);
			System.out.println(gpac.toString());
			System.out.println("");
			Repository repo = CooperationHelper.openExistingRepo(pth);
			gpac = new PrjtCommits(repo);
			for(Commit c: gpac.getCommits()) {
				System.out.println(c.getCommitId());
			}
			System.out.println(gpac.getRepo());
			gpac = new PrjtCommits(pth2);
			System.out.println(gpac.getRepo());
		} catch (IOException | GitAPIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetAllCommitsFiles() {
		String pth = "D:/work/GitMining/Repos/TechTest";
		try {
			gpac = new PrjtCommits("");
			gpac.getCommits();
			
		} catch (IOException | GitAPIException e) {
			e.printStackTrace();
		}
		
	}
}
