package iSESniper.cooperation.commit;

import java.io.IOException;
import java.util.List;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.junit.Test;

import iSESniper.cooperation.commit.extractor.PrjtCommits;
import iSESniper.cooperation.entity.Commit;

/**
 * @author : Magister
 * @fileName : iSESniper.cooperation.commit.TestCommitDiff.java
 * 
 *           2018年3月7日 下午9:28:40
 */

public class TestCommitDiff {

	CommitDiff cd;

	@Test
	public void testOutputDiff() {
		try {
			PrjtCommits pcs = new PrjtCommits("");
			List<Commit> cs = pcs.getCommits();
			for (int i = 0; i < cs.size() - 1; i++) {
				cd = new CommitDiff(pcs.getRepository(), cs.get(i + 1), cs.get(i));
				cd.outputDiff();
			}
		} catch (IOException | GitAPIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testOutputSingleCommitFilesContent() throws NoHeadException, IOException, GitAPIException {
		PrjtCommits pcs = new PrjtCommits("");
		List<Commit> cs = pcs.getCommits();
		cd = new CommitDiff(pcs.getRepository());
		Commit c = cs.get(cs.size() - 1);
		String cid = c.getCommitId();
		cd.outputSingleCommitFilesContent(cid);
		System.out.println(c.getMessage());
	}
}
