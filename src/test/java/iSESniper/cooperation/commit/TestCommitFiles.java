package iSESniper.cooperation.commit;

import java.io.IOException;

import org.eclipse.jgit.lib.Repository;
import org.junit.Test;

import iSESniper.cooperation.helper.CooperationHelper;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.cooperation.commit.TestCommitFiles.java
 *  
 *  2018年3月10日	下午11:58:07  
*/

public class TestCommitFiles {

	@Test
	public void testCommitFiles() throws IOException {
		String newId = "85f47b6d3ed8cea2616430c92920105c237c672c";
		String oldId = "7a776650c22dc455f8181d41e64764a9bae4d59f";
		String originId = "a4d92e43422de431ff519ab8d7fc8e9bb59d2f5f";
		Repository repo = CooperationHelper.openExistingRepo("D:\\Study\\lesions\\SCII\\SECII\\SECC2");
		
		System.out.println(CommitFiles.getCommitFiles(repo, newId+"^", newId));
//		System.out.println(CommitFiles.readFileContentFromCommit(repo, originId, "README.md"));
	}
}
