package iSESniper.cooperation.helper;

import java.io.IOException;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.junit.Test;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.cooperation.helper.RepositoryDownloaderTest.java
 *  
 *  2018年4月16日	下午7:59:07  
*/

public class RepositoryDownloaderTest {

	String REMOTE_URL = "http://114.215.188.21/ljt/GitStudy.git";
	String REMOTE_URL1 = "http://114.215.188.21/ljt/GitTest2.git";
	String BASE_FOLDER = "D:\\work\\GitMining\\Repos\\samples";
	String user = "ljt";
	String pwd = "git#ljt16";
	
	@Test
	public void testCloneRepo() {
		RepositoryDownloader repoDownloader = new RepositoryDownloader(BASE_FOLDER, user, pwd);
		try {
			System.out.println(repoDownloader.cloneRemoteRepo(REMOTE_URL));
		} catch (GitAPIException | IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCloneRepos() {
		String[] urls = {REMOTE_URL, REMOTE_URL1};
		RepositoryDownloader repositoryDownloader = new RepositoryDownloader(BASE_FOLDER, user, pwd);
		try {
			System.out.println(repositoryDownloader.cloneRemoteRepos(urls));
		} catch (GitAPIException | IOException e) {
			e.printStackTrace();
		}
	}
}
