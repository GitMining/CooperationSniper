package iSESniper.cooperation.helper;

import iSESniper.cooperation.exception.FilesListIsEmptyException;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author :   Magister
 * @fileName :   iSESniper.cooperation.helper.CooperationHelperTest.java
 * <p>
 * 2018年2月24日	下午5:34:19
 */

public class CooperationHelperTest {

    private String pth = "D:\\work\\GitMining\\Repos\\samples\\jgit-cookbook";
    private String pth2 = "D:\\work\\GitMining\\Repos\\CooperationSniper";
    private String vainRepo = "D:\\work\\GitMining\\Repos\\samples\\141250103_GroupXTags\\XTags_Phase_III";

    @Test
    public void testOpenExistingRepo() {
        try {
            FileRepositoryBuilder builder = new FileRepositoryBuilder();
            Repository repo1 = CooperationHelper.openExistingRepo("");
            Repository repo2 = CooperationHelper.openExistingRepo(pth);
            Repository repo3 = CooperationHelper.openExistingRepo();
            Repository repo4 = builder.readEnvironment().findGitDir().build();
            System.out.println(repo1);
            System.out.println(repo2);
            System.out.println(repo3);
            System.out.println(repo4);
//			Git git1 = new Git(repo1);
            @SuppressWarnings("resource")
            Git git2 = new Git(repo2);
//			Git git3 = new Git(repo3);
            @SuppressWarnings("resource")
            Git git4 = new Git(repo4);
            Iterable<RevCommit> logs2 = git2.log().addPath(".classpath").call();
            Iterable<RevCommit> logs4 = git4.log().addPath("GetFileAllCommits.java").call();
            for (RevCommit c : logs2) {
                System.out.print("[git2 Commit] " + c + ", AuthorIdent: " + c.getAuthorIdent() + ", Message: "
                        + c.getFullMessage());
            }
            for (RevCommit c : logs4) {
                System.out.print("[git4 Commit] " + c + ", AuthorIdent: " + c.getAuthorIdent() + ", Message: "
                        + c.getFullMessage());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoHeadException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (GitAPIException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void testScan() {
        List<String> pths = new ArrayList<String>();
        pths = CooperationHelper.getAllFiles(pth2);
        int count = 0;
        for (String p : pths) {
            count++;
            System.out.println(p);
        }
        System.out.println("The number of files is: " + count);
    }

    @Test
    public void testGetAllFilesRltPth() {
        Collection<String> exfs = new ArrayList<String>();
        List<String> pths1 = CooperationHelper.getAllFilesRltPth(pth2, exfs);
        int count = 0;
        for (String p : pths1) {
            count++;
            System.out.println(p);
        }
        System.out.println("The number of files is: " + count);

        List<String> pths2 = CooperationHelper.getAllFilesRltPth(pth2, exfs);
        count = 0;
        for (String p : pths2) {
            count++;
            System.out.println(p);
        }
        System.out.println("The number of files is: " + count);
    }

    @Test
    public void testGetAllJavaFiles() {
        try {
            List<String> pths1 = CooperationHelper.getAllJavaFiles(pth2);
            int count = 0;
            for (String p : pths1) {
                count++;
                System.out.println(p);
            }
            System.out.println("The number of files is: " + count);
        } catch (FilesListIsEmptyException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCatchNoHeadException() throws IOException {
        try (Repository repository = CooperationHelper.openExistingRepo(vainRepo)) {
            CooperationHelper.getCommitList(repository,"");
        } catch (NoHeadException e) {
            System.out.println("U need a HEAD.:)");
        } catch (GitAPIException e) {
            e.printStackTrace();
        }
    }
}
