package iSESniper.cooperation.exception;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.cooperation.exception.ParametersAbsenceException.java
 *  
 *  2018年3月9日	上午12:48:33  
*/

public class ParametersAbsenceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ParametersAbsenceException(String method) {
		super("[" + method + "] some parameters are missing.");
	}

}
