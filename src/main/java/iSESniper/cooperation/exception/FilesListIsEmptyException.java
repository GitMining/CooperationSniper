package iSESniper.cooperation.exception;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.cooperation.exception.FilesListIsEmptyException.java
 *  
 *  2018年3月22日	下午2:48:30  
*/

public class FilesListIsEmptyException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7871179058002873614L;

	public FilesListIsEmptyException(String e) {
		super(e);
	}
}
