package iSESniper.cooperation.helper;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

/**
 * @author : Magister
 * @fileName : iSESniper.cooperation.helper.RepositoryDownloader.java
 * 
 *           2018年4月16日 下午7:44:41
 */

public class RepositoryDownloader {

	private String baseFolder;

	// This method is not safe.
	private String user;

	private String pwd;

	public RepositoryDownloader(String baseFolder, String user, String pwd) {
		super();
		this.baseFolder = baseFolder;
		this.user = user;
		this.pwd = pwd;
	}

	public int cloneRemoteRepos(String[] urls)
			throws InvalidRemoteException, TransportException, GitAPIException, IOException {
		int count = 0;
		for (int i = 0; i < urls.length; i++) {
			if(cloneRemoteRepo(urls[i]))
				count++;
		}
		return count;
	}

	public boolean cloneRemoteRepo(String url)
			throws InvalidRemoteException, TransportException, GitAPIException, IOException {
		boolean flag = false;
		// prepare a new folder for the cloned repository.
		String regEx = "/[A-Za-z0-9_-]*/[A-Za-z0-9_-]*\\.git$";
		Pattern ptn = Pattern.compile(regEx);
		Matcher mtch = ptn.matcher(url);
		String newFolder = "";
		if (mtch.find())
			newFolder = mtch.group(0);
		newFolder = newFolder.substring(0, newFolder.lastIndexOf("."));
		File localPath = new File(baseFolder + newFolder);
		if (!localPath.exists())
			localPath.mkdir();
		else {
			CooperationHelper.delFolder(localPath.getCanonicalPath());
			localPath.mkdir();
		}
		// Then clone.
		CredentialsProvider credentialsProvider = new UsernamePasswordCredentialsProvider(user, pwd);
		try (Git result = Git.cloneRepository().setURI(url).setDirectory(localPath)
				.setCredentialsProvider(credentialsProvider).call()) {
			// Note: the call() returns an opened repository already which needs to be
			// closed to avoid file handle leaks!
			System.out.println("Having repository: " + result.getRepository().getDirectory());
			if (result != null)
				flag = true;
		}
		return flag;
	}
}
