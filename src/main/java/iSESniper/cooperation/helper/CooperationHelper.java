package iSESniper.cooperation.helper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;

import iSESniper.cooperation.entity.Commit;
import iSESniper.cooperation.exception.FilesListIsEmptyException;

/**
 * @author : Magister
 * @fileName : iSESniper.cooperation.helper.CooperationHelper.java
 * 
 *           2018年2月24日 下午5:25:06
 */

public abstract class CooperationHelper {

	public static void delFolder(String folderPath) {
		try {
			delAllFiles(folderPath); // 删除完里面所有内容
			String filePath = folderPath;
			filePath = filePath.toString();
			java.io.File myFilePath = new java.io.File(filePath);
			myFilePath.delete(); // 删除空文件夹
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean delAllFiles(String pth) {
		boolean flag = false;
		File file = new File(pth);
		if (!file.exists())
			return flag;
		if (!file.isDirectory())
			return flag;
		String[] tempList = file.list();
		File temp = null;
		for (int i = 0; i < tempList.length; i++) {
			if (pth.endsWith(File.separator)) {
				temp = new File(pth + tempList[i]);
			} else {
				temp = new File(pth + File.separator + tempList[i]);
			}
			if (temp.isFile())
				temp.delete();
			if (temp.isDirectory()) {
				delAllFiles(pth + "/" + tempList[i]);
				delFolder(pth + "/" + tempList[i]);
				flag = true;
			}
		}
		return flag;
	}

	public static Repository openExistingRepo(String pth) throws IOException {
		if (pth.trim().equals(""))
			return openExistingRepo();
		File f = new File(pth + "/.git");
		FileRepositoryBuilder builder = new FileRepositoryBuilder();
		return builder.setGitDir(f).build();
	}

	public static Repository openExistingRepo() throws IOException {
		FileRepositoryBuilder builder = new FileRepositoryBuilder();
		return builder.readEnvironment().findGitDir().build();
	}

	public static AbstractTreeIterator prepareTreeParser(Repository repo, String cid) throws IOException {
		try (RevWalk walk = new RevWalk(repo)) {
			RevCommit commit = walk.parseCommit(ObjectId.fromString(cid));
			RevTree tree = walk.parseTree(commit.getTree().getId());

			CanonicalTreeParser treeParser = new CanonicalTreeParser();
			try (ObjectReader reader = repo.newObjectReader()) {
				treeParser.reset(reader, tree.getId());
			}
			walk.dispose();
			return treeParser;
		}
	}

	public static List<String> getAllFiles(String pth) {
		List<String> files = new ArrayList<String>();
		File fileDir = new File(pth);
		if (!fileDir.isDirectory()) {
			// System.out.println(fileDir.getAbsolutePath());
			files.add(fileDir.getPath());
		} else {
			String[] subFiles = fileDir.list();
			for (int i = 0; i < subFiles.length; i++) {
				String subPth = pth + "\\" + subFiles[i];
				files.addAll(getAllFiles(subPth));
			}
		}
		return transferSlash(files);
	}

	public static List<String> getAllJavaFiles(String pth) throws FilesListIsEmptyException {
		List<String> files = getAllFilesRltPth(pth);
		if (files.isEmpty()) {
			throw new FilesListIsEmptyException("OPPS! Target file list is empty.");
		}
		ArrayList<String> jFiles = new ArrayList<String>();
		String regEx = "\\.java$";
		Pattern ptn = Pattern.compile(regEx);
		Matcher mtch;
		for (String file : files) {
			mtch = ptn.matcher(file);
			if (mtch.find()) {
				jFiles.add(file);
				// System.out.println(file);
			}
		}
		return jFiles;
	}

	public static List<String> getAllFilesRltPth(String pth) {
		List<String> files = new ArrayList<String>();
		File fileDir = new File(pth);
		File[] subFs = fileDir.listFiles();
		for (int i = 0; i < subFs.length; i++) {
			if (subFs[i].isFile()) {
				files.add(subFs[i].getName());
			} else if (subFs[i].isDirectory()) {
				String parent = subFs[i].getName();
				List<String> subs = getAllFilesRltPth(pth + "\\" + parent);
				for (String s : subs) {
					files.add(parent + "\\" + s);
				}
			}
		}
		return transferSlash(files);
	}

	public static List<String> getAllFilesRltPth(String pth, Collection<String> ignore) {
		Collection<String> excludeFolder = new ArrayList<String>();
		excludeFolder.add(".git");
		excludeFolder.add("target");
		// excludeFolder.add(".gitignore");
		excludeFolder.add(".settings");
		excludeFolder.add(".project");
		if (!ignore.isEmpty()) {
			for (String s : ignore) {
				if (!excludeFolder.contains(s))
					excludeFolder.add(s);
			}
		}
		List<String> files = getRltFiles(pth, excludeFolder, 0);
		return transferSlash(files);
	}

	public static boolean createNewFolder(String commit) {
		/**
		 * Create folder named by commit id under the project first level folder
		 * prjt/out/.
		 * 
		 * @param commit
		 *            : certain commit name.
		 */
		File out = new File("output");
		if (!out.exists()) {
			if (out.mkdir())
				System.out.println("Init output folder sucessfully.");
			else {
				System.out.println("Init output folder failed.");
				return false;
			}
		}
		File folder = new File("output/" + commit);
		if (!folder.exists()) {
			if (folder.mkdir())
				System.out.println("Init commit folder sucessfully.");
			else
				return false;
		}
		return true;
	}

	public static File createCommitFile(String commit, String f) throws IOException {
		File file = new File("output/" + commit + "/" + f);
		File fp = file.getParentFile();
		if (!fp.exists())
			fp.mkdirs();
		if (file.createNewFile())
			System.out.println("Create commit file " + f + " successfully.");
		return file;
	}

	public static List<Commit> getCommitList(Repository repo, String pth)
			throws NoHeadException, GitAPIException, IOException {
		List<Commit> commits = new ArrayList<Commit>();
		try (Git git = new Git(repo);) {
			Iterable<RevCommit> logs = pth.equals("") ? git.log().call() : git.log().addPath(pth).call();
			for (RevCommit c : logs) {
				Commit element = setCommitBasicInfo(c);
				RevCommit[] parents = c.getParents();
				for (RevCommit commit : parents) {
					Commit parent = setCommitBasicInfo(commit);
					element.getParents().add(parent);
				}
				commits.add(element);
			}
			return commits;
		}
	}

	private static Commit setCommitBasicInfo(RevCommit c) {
		String commitId = c.getId().getName();
		String author = c.getAuthorIdent().getName();
		String email = c.getAuthorIdent().getEmailAddress();
		String message = c.getFullMessage();
		long when = c.getCommitTime();
		TimeZone timeZone = c.getAuthorIdent().getTimeZone();
		List<Commit> parents = new ArrayList<>();
		Commit commit = new Commit(commitId, author, email, when, timeZone, message);
		commit.setParents(parents);
		return commit;
	}

	public static List<Commit> getCommitList(String pth) throws IOException, NoHeadException, GitAPIException {
		List<Commit> commits = new ArrayList<Commit>();
		try (Repository repository = openExistingRepo(pth)) {
			commits = getCommitList(repository, "");
			return commits;
		}
	}

	private static List<String> getRltFiles(String pth, Collection<String> ignore, int deep) {
		List<String> files = new ArrayList<String>();
		File fileDir = new File(pth);
		File[] subFs = fileDir.listFiles();
		for (int i = 0; i < subFs.length; i++) {
			if (deep == 0 && ignore.contains(subFs[i].getName()))
				continue;
			if (subFs[i].isFile()) {
				files.add(subFs[i].getName());
			} else if (subFs[i].isDirectory()) {
				String parent = subFs[i].getName();
				List<String> subs = getRltFiles(pth + "\\" + parent, ignore, deep + 1);
				for (String s : subs) {
					files.add(parent + "\\" + s);
				}
			}
		}
		return transferSlash(files);
	}

	private static List<String> transferSlash(Collection<String> ss) {
		List<String> ns = new ArrayList<String>();
		for (String s : ss) {
			ns.add(s.replace('\\', '/'));
		}
		return ns;
	}

	public static boolean fileTypeIsLegal(String name, List<String> fileTypes) {
		boolean flag = false;
		int idx = name.lastIndexOf('.');
		if (idx < 0)
			return flag;
		String type = name.substring(idx);
		if (fileTypes.contains(type))
			flag = true;
		return flag;
	}

}
