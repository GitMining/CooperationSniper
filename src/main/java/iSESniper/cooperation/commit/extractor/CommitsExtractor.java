package iSESniper.cooperation.commit.extractor;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;

import iSESniper.cooperation.commit.CommitFiles;
import iSESniper.cooperation.entity.Author;
import iSESniper.cooperation.entity.Commit;
import iSESniper.cooperation.entity.FileDiff;
import iSESniper.cooperation.helper.CooperationHelper;

/**
 * @author : Magister
 * @fileName : iSESniper.cooperation.commit.extractor.CommitsExtractor.java
 * 
 *           2018年4月17日 上午11:19:49
 */

public class CommitsExtractor {

	private String repo;

	private List<Commit> commits;

	public CommitsExtractor(String repo) {
		super();
		this.repo = repo;
	}

	public List<Commit> getCommits() throws NoHeadException, IOException, GitAPIException {
		if(commits == null)
			extractProjectCommits();
		return commits;
	}

	public int extractProjectCommits() throws IOException, NoHeadException, GitAPIException {
		int count = 0;
		// Extract basic commit information.
		commits = CooperationHelper.getCommitList(repo);
		// Set Files base info in commit;
		setCommitFilesInfo();
		count = commits.size();
		return count;
	}
	
	public Set<Author> getAuthors(){
		Set<Author> authors = new HashSet<Author>();
		if(commits == null)
			return authors;
		for(Commit c: commits) {
			authors.add(c.getAuthor());
		}
		return authors;
	}

	private void setCommitFilesInfo() throws IOException, GitAPIException {
		if (commits == null)
			return;
		int len = commits.size();
		for (int i = 0; i < len - 1; i++) {
			String nc = commits.get(i).getCommitId();
			String oc = commits.get(i).getParents().get(0).getCommitId();
			List<FileDiff> fileDiffs = CommitFiles.getCommitFiles(repo, oc, nc);
			commits.get(i).setFiles(fileDiffs);
		}
		commits.get(commits.size() - 1)
				.setFiles(CommitFiles.getSingleCommitFiles(repo, commits.get(commits.size() - 1).getCommitId()));
	}

	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("Extracted Commits as Below \nrepo: ");
		sBuffer.append(repo);
		sBuffer.append(", commits: \n");
		if (commits == null || commits.size() <= 0) {
			sBuffer.append("Commit List is empty, or not execute extractor;");
			return sBuffer.toString();
		}
		for (Commit c : commits) {
			sBuffer.append(c.toString());
		}
		return sBuffer.toString();
	}

}
