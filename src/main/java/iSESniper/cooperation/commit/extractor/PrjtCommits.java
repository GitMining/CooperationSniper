package iSESniper.cooperation.commit.extractor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.lib.Repository;

import iSESniper.cooperation.commit.CommitFiles;
import iSESniper.cooperation.entity.Commit;
import iSESniper.cooperation.entity.FileDiff;
import iSESniper.cooperation.helper.CooperationHelper;

/**
 * @author : Magister
 * @fileName : iSESniper.cooperation.commit.GetPrjtAllCommits.java
 * @deprecated
 *           2018年3月5日 下午5:42:14
 */

public class PrjtCommits {

	private String repo;

	private List<Commit> commits;

	private Repository repository;

	public PrjtCommits(String repo) throws IOException, NoHeadException, GitAPIException {
		this.repo = repo;
		getAllCommits();
		if (this.repo.equals(""))
			this.repo = this.repository.getDirectory().getParent();
	}

	public PrjtCommits(Repository repo) throws NoHeadException, GitAPIException, IOException {
		this.repository = repo;
		this.repo = repo.getDirectory().getParent();
		this.commits = CooperationHelper.getCommitList(repo, "");
		getAllCommitsFiles();
	}

	public String getRepo() {
		return repo.substring(repo.lastIndexOf("\\") + 1);
	}

	public List<Commit> getCommits() {
		return commits;
	}

	public String getRepositoryDir() {
		return this.repository.getDirectory().toString();
	}

	public Repository getRepository() {
		return this.repository;
	}

	private void getAllCommits() throws IOException, NoHeadException, GitAPIException {
		this.commits = new ArrayList<Commit>();
		this.repository = CooperationHelper.openExistingRepo(this.repo);
		this.commits = CooperationHelper.getCommitList(repository, "");
		getAllCommitsFiles();
	}

	private void getAllCommitsFiles() throws IOException {
		if (this.commits == null)
			return;
		int i = 0;
		for (; i < this.commits.size() - 1; i++) {
			Commit nc = this.commits.get(i);
			Commit oc = this.commits.get(i + 1);
			List<FileDiff> files = CommitFiles.getCommitFiles(repository, oc.getCommitId(), nc.getCommitId());
			nc.setFiles(files);
			System.out.println(nc);
			System.out.println(nc.getFiles());
		}
		Commit origin = this.commits.get(i);
		origin.setFiles(CommitFiles.getSingleCommitFiles(repository, origin.getCommitId()));
		System.out.println(origin);
		System.out.println(origin.getFiles());
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("[ProjectAllCommits] [repo: " + repo + ", commits:\n");
		if (commits.isEmpty() || commits == null)
			sb.append("null");
		else {
			for (Commit c : commits) {
				sb.append(c.toString() + '\n');
			}
		}
		sb.append("Commits count: " + this.commits.size());
		return sb.toString();
	}
}
