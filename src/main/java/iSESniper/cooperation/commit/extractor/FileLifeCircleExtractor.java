package iSESniper.cooperation.commit.extractor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import iSESniper.cooperation.commit.enumeration.Type;
import iSESniper.cooperation.entity.Action;
import iSESniper.cooperation.entity.Author;
import iSESniper.cooperation.entity.Commit;
import iSESniper.cooperation.entity.FileDiff;
import iSESniper.cooperation.entity.FileLifeCircle;
import iSESniper.cooperation.helper.CooperationHelper;

/**
 * @author : Magister
 * @fileName :
 *           iSESniper.cooperation.commit.extractor.FileLifeCircleExtractor.java
 * 
 *           2018年4月25日 下午1:47:58
 */

public class FileLifeCircleExtractor {

	private String repo;

	private List<FileLifeCircle> fileLifeCircles;

	public String getRepo() {
		return repo;
	}

	public List<FileLifeCircle> getFileLifeCircles() {
		return fileLifeCircles;
	}

	public FileLifeCircleExtractor(String repo) {
		super();
		this.repo = repo;
	}

	/**
	 * Get the life circle of repository's files.
	 * 
	 * @param commits
	 * @param fileTypes
	 * @return the number of files that added into repository.
	 */
	public int extractFileLifeCircle(List<Commit> commits, List<String> fileTypes) {
		if (fileTypes == null || fileTypes.size() == 0) {
			fileTypes = new ArrayList<String>();
			fileTypes.add(".java");
			fileTypes.add(".js");
		}
		int count = 0;
		fileLifeCircles = new ArrayList<FileLifeCircle>();
		FileLifeCircle fileLifeCircle;
		int i = commits.size() - 1;
		for (; i >= 0; i--) {
			Commit c = commits.get(i);
			Author author = c.getAuthor();
			long time = c.getWhen();
			List<FileDiff> diffs = c.getFiles();
			for (FileDiff fDiff : diffs) {
				Type changeType = fDiff.getChangeType();
				String name;
				if (changeType == Type.DELETE)
					name = fDiff.getOldPth();
				else
					name = fDiff.getNewPth();
				if (!CooperationHelper.fileTypeIsLegal(name, fileTypes))
					continue;
				long added = fDiff.getAdded();
				long removed = fDiff.getRemoved();
				TimeZone timeZone = c.getTimeZone();
				fileLifeCircle = new FileLifeCircle();
				fileLifeCircle.setFileName(name);
				int idx = fileLifeCircles.indexOf(fileLifeCircle);
				if (idx < 0 && changeType != Type.RENAME) {
					// This file is new added into repository.
					fileLifeCircle.setActions(new ArrayList<Action>());
					fileLifeCircle.getActions().add(new Action(author, added, removed, time, changeType, timeZone));
					fileLifeCircles.add(fileLifeCircle);
					count++;
				} else if (idx < 0 && changeType == Type.RENAME) {
					// This file is renamed, so we need to update the file name to new name.
					String oldName = fDiff.getOldPth();
					fileLifeCircle.setFileName(oldName);
					idx = fileLifeCircles.indexOf(fileLifeCircle);
					fileLifeCircles.get(idx).setFileName(name);
					List<Action> actions = fileLifeCircles.get(idx).getActions();
					actions.add(new Action(author, added, removed, idx, changeType, timeZone));
				} else {
					List<Action> actions = fileLifeCircles.get(idx).getActions();
					actions.add(new Action(author, added, removed, time, changeType, timeZone));
				}
			}
		}
		for (FileLifeCircle lifeCircle : fileLifeCircles) {
			countAuthorRatio(lifeCircle);
		}
		return count;
	}

	private void countAuthorRatio(FileLifeCircle fileLifeCircle) {
		List<Action> actions = fileLifeCircle.getActions();
		Map<Author, Integer> ratios = new HashMap<Author, Integer>();
		for (Action action : actions) {
			Author author = action.getAuthor();
			Integer num = ratios.get(author);
			num = num == null ? 1 : num + 1;
			ratios.put(author, num);
		}
	}

}
