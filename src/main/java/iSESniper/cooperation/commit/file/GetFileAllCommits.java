package iSESniper.cooperation.commit.file;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.lib.Repository;

import iSESniper.cooperation.entity.Commit;
import iSESniper.cooperation.entity.FileCommits;
import iSESniper.cooperation.exception.FilesListIsEmptyException;
import iSESniper.cooperation.helper.CooperationHelper;

/**
 * @author : Magister
 * @fileName : iSESniper.cooperation.commit.file.GetFileAllCommits.java
 * 
 *           2018年2月25日 下午12:47:32
 */

public class GetFileAllCommits {

	private String repo;

	private Repository repository;

	@Deprecated
	public GetFileAllCommits(String repo) {
		this.repo = repo;
	}

	public GetFileAllCommits(Repository repo) {
		this.repository = repo;
		this.repo = repository.getDirectory().getName();
	}

	public FileCommits getAllCommits(String pth) throws IOException, NoHeadException, GitAPIException {
		FileCommits fcs;
		List<Commit> commits = new ArrayList<Commit>();
		if (this.repository == null) {
			try (Repository repo = CooperationHelper.openExistingRepo(this.repo);) {
				commits = CooperationHelper.getCommitList(repo, pth);
				fcs = new FileCommits(pth, commits);
				return fcs;
			}
		}
		else {
			commits = CooperationHelper.getCommitList(this.repository, pth);
			fcs = new FileCommits(pth, commits);
			return fcs;
		}
	}

	public Map<String, Integer> getAuthorCommitPercent(FileCommits fCommits) {
		Map<String, Integer> result = new HashMap<String, Integer>();
		for (Commit c : fCommits.getCommits()) {
			Integer times = result.get(c.getAuthor());
			if (times == null)
				result.put(c.getAuthor().toString(), 1);
			else
				result.put(c.getAuthor().toString(), times + 1);
		}
		return result;
	}

	public String getPrjtName() {
		return this.repo.substring(this.repo.lastIndexOf('\\') + 1);
	}

	public static void main(String[] args) {
		String path = "D:\\work\\GitMining\\Repos\\QualitySniper";
		// String path = "D:\\work\\GitMining\\Repos\\samples\\jgit-cookbook";
		try {
			GetFileAllCommits gfac = new GetFileAllCommits(path);
			List<String> pths;
			// Collection<String> exfs = new ArrayList<String>();
			// exfs.add("pmd");
			// exfs.add("out");
			// exfs.add(".classpath");
			// exfs.add("gradle");
			// exfs.add("httpserver");
			// pths = CooperationHelper.getAllFilesRltPth(path, exfs);
			pths = CooperationHelper.getAllJavaFiles(path);
			int fscount = 0;
			System.out.println("");
			for (String pth : pths) {
				FileCommits fcs = gfac.getAllCommits(pth);
				System.out.println(fcs);
				gfac.getAuthorCommitPercent(fcs);
				fscount++;
			}
			System.out.println("This repo has " + fscount + " files.");
		} catch (NoHeadException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GitAPIException e) {
			e.printStackTrace();
		} catch (FilesListIsEmptyException e) {
			e.printStackTrace();
		}
	}
}
