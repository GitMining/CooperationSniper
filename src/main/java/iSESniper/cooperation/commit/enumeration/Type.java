package iSESniper.cooperation.commit.enumeration;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.cooperation.commit.enumeration.Type.java
 *  
 *  2018年3月7日	下午9:01:16  
*/

public enum Type {
	ADD, MODIFY, DELETE, RENAME, COPY;
}
