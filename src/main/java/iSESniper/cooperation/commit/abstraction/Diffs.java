package iSESniper.cooperation.commit.abstraction;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.cooperation.commit.Diffs.java
 *  
 *  2018年3月7日	下午8:57:07  
*/

public interface Diffs {
	public void outputDiff();
}
