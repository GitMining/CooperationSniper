package iSESniper.cooperation.commit;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathSuffixFilter;

import iSESniper.cooperation.commit.abstraction.Diffs;
import iSESniper.cooperation.entity.Commit;
import iSESniper.cooperation.exception.ParametersAbsenceException;
import iSESniper.cooperation.helper.CooperationHelper;

/**
 * @author : Magister
 * @fileName : iSESniper.cooperation.commit.CommitDiff.java
 * @deprecated
 *           2018年3月7日 下午8:54:27
 */

public class CommitDiff implements Diffs {

	private Commit oldCommit;

	private Commit newCommit;

	private Repository repo;

	public CommitDiff(Repository repo, Commit oldCommit, Commit newCommit) {
		this.repo = repo;
		this.oldCommit = oldCommit;
		this.newCommit = newCommit;
	}
	
	public CommitDiff(Repository repo) {
		this.repo = repo;
	}

	@Override
	public void outputDiff() {
		if (oldCommit == null || newCommit == null)
			try {
				throw new ParametersAbsenceException(Thread.currentThread().getStackTrace()[2].getMethodName());
			} catch (ParametersAbsenceException e1) {
				e1.printStackTrace();
			}
		String oldc = oldCommit.getCommitId();
		String newc = newCommit.getCommitId();
		try {
			AbstractTreeIterator oldTreeParser = CooperationHelper.prepareTreeParser(repo, oldc);
			AbstractTreeIterator newTreeParser = CooperationHelper.prepareTreeParser(repo, newc);
			CooperationHelper.createNewFolder(newc);
			try (Git git = new Git(repo)) {
				List<DiffEntry> diffs = git.diff().setOldTree(oldTreeParser).setNewTree(newTreeParser)
						.setPathFilter(PathSuffixFilter.create(".java")).call();
				for (DiffEntry entry : diffs) {
					String pth = entry.getNewPath();
					File out = CooperationHelper.createCommitFile(newc, pth);
					FileOutputStream output = new FileOutputStream(out);
					System.out.println("Entry: " + entry + ", from: " + entry.getOldId() + ", to: " + entry.getNewId());
					try (DiffFormatter formatter = new DiffFormatter(output)) {
						formatter.setRepository(repo);
						formatter.format(entry);
					}
				}
			} catch (GitAPIException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void outputSingleCommitFilesContent(String cid) throws IOException {
		CooperationHelper.createNewFolder(cid);
		try (RevWalk revWalk = new RevWalk(repo)) {
			RevCommit commit = revWalk.parseCommit(ObjectId.fromString(cid));
			RevTree tree = commit.getTree();
			try (TreeWalk treeWalk = new TreeWalk(repo)) {
				treeWalk.addTree(tree);
				treeWalk.setRecursive(true);
				while (treeWalk.next()) {
					File f = CooperationHelper.createCommitFile(cid, treeWalk.getPathString());
					FileOutputStream output = new FileOutputStream(f);
					ObjectId oId = treeWalk.getObjectId(0);
					ObjectLoader loader = repo.open(oId);
					loader.copyTo(output);
				}
			}
		}
	}
}
