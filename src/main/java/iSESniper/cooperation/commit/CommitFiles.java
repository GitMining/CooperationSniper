package iSESniper.cooperation.commit;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffConfig;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffEntry.ChangeType;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.errors.AmbiguousObjectException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.RevisionSyntaxException;
import org.eclipse.jgit.lib.Config;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.FollowFilter;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;

import iSESniper.cooperation.commit.enumeration.Type;
import iSESniper.cooperation.entity.FileDiff;
import iSESniper.cooperation.helper.CooperationHelper;

/**
 * @author : Magister
 * @fileName : iSESniper.cooperation.commit.CommitFiles.java
 * 
 *           2018年3月10日 下午10:43:00
 */

public abstract class CommitFiles {

	/**
	 * This method help to acquire the list of all changed files between two commits
	 * by using repository's full name, old commit hash Id and new commit hash Id.
	 * 
	 * @param repo
	 *            repository full name.
	 * @param oldc
	 *            old commit hash id.
	 * @param newc
	 *            new commit hash id.
	 * @return The list of changed files.
	 * @throws IOException
	 */
	public static List<FileDiff> getCommitFiles(String repo, String oldc, String newc) throws IOException {
		List<FileDiff> files;
		try (Repository repository = CooperationHelper.openExistingRepo(repo)) {
			files = getCommitFiles(repository, oldc, newc);
		}

		return files;
	}

	/**
	 * This method help to acquire the list of all changed files between two commits
	 * by using repository's full name, old commit hash Id and new commit hash Id.
	 * 
	 * @param repo
	 *            repository instance.
	 * @param oldc
	 *            old commit hash id.
	 * @param newc
	 *            new commit hash id.
	 * @return The list of changed files.
	 * @throws IOException
	 */
	public static List<FileDiff> getCommitFiles(Repository repo, String oldc, String newc) {
		List<FileDiff> files = new ArrayList<FileDiff>();
		try {
			AbstractTreeIterator oldTreeParser = CooperationHelper.prepareTreeParser(repo, oldc);
			AbstractTreeIterator newTreeParser = CooperationHelper.prepareTreeParser(repo, newc);
			Config config = new Config();
			config.setBoolean("diff", null, "renames", true);
			DiffConfig diffConfig = config.get(DiffConfig.KEY);
			try (Git git = new Git(repo)) {
				List<DiffEntry> diffs = git.diff().setOldTree(oldTreeParser).setNewTree(newTreeParser).call();
				List<String> renameOldPthList = new ArrayList<>();
				StringBuffer sBuffer = new StringBuffer();
				for (DiffEntry entry : diffs) {
					try (OutputStream oStream = new ByteArrayOutputStream();
							DiffFormatter formatter = new DiffFormatter(oStream)) {
						String newPth = entry.getNewPath();
						String oldPth = entry.getOldPath();
						Type changeType = Type.valueOf(entry.getChangeType().toString());
						
						if (changeType == Type.ADD) {
							List<DiffEntry> diffEntrys = git.diff()
									// A RevWalk instance can only be used once to generate results. Running a
									// second time requires creating a new RevWalk instance
									.setOldTree(CooperationHelper.prepareTreeParser(repo, oldc))
									.setNewTree(CooperationHelper.prepareTreeParser(repo, newc))
									.setPathFilter(FollowFilter.create(newPth, diffConfig)).call();
							if (diffEntrys.size() > 0) {
								DiffEntry diffEntry = diffEntrys.get(0);
								if (diffEntry.getChangeType() == ChangeType.RENAME) {
									changeType = Type.RENAME;
									oldPth = diffEntry.getOldPath();
									renameOldPthList.add(oldPth);
									FileDiff cFile = new FileDiff(changeType, oldPth, newPth);
									addFile2FilesList(repo, sBuffer, oStream, formatter, diffEntry, files, cFile);
									continue;
								}
							}
						}

						if (changeType == Type.DELETE && renameOldPthList.contains(oldPth))
							continue;

						FileDiff cFile = new FileDiff(changeType, oldPth, newPth);
						addFile2FilesList(repo, sBuffer, oStream, formatter, entry, files, cFile);
					}
				}
			} catch (GitAPIException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return files;
	}

	

	private static void addFile2FilesList(Repository repo, StringBuffer sBuffer, OutputStream oStream,
			DiffFormatter formatter, DiffEntry entry, List<FileDiff> files, FileDiff cFile) throws IOException {
		formatter.setRepository(repo);
		formatter.format(entry);
		sBuffer.append(oStream.toString());
		setModifiedRowsCount(cFile, sBuffer, false);
		sBuffer.delete(0, sBuffer.length() - 1);
		files.add(cFile);
	}

	/**
	 * Acquire the list of all files in a single commit. U can capture all files in
	 * original commit through it.
	 * 
	 * @param repo
	 *            repository full name.
	 * @param cId
	 *            target commit hash id.
	 * @return the list of all files in target commit.
	 * @throws IOException
	 */
	public static List<FileDiff> getSingleCommitFiles(String repo, String cId) throws IOException {
		List<FileDiff> files;
		try (Repository repository = CooperationHelper.openExistingRepo(repo)) {
			files = getSingleCommitFiles(repository, cId);
		}
		return files;
	}

	/**
	 * Acquire the list of all files in a single commit. U can capture all files in
	 * original commit through it.
	 * 
	 * @param repo
	 *            repository instance.
	 * @param cId
	 *            target commit hash id.
	 * @return the list of all files in target commit.
	 * @throws IOException
	 */
	public static List<FileDiff> getSingleCommitFiles(Repository repo, String cId) throws IOException {
		/**
		 * This method return the file list that included in a special commit.
		 */
		List<FileDiff> files = new ArrayList<FileDiff>();
		try (RevWalk revWalk = new RevWalk(repo)) {
			RevCommit commit = revWalk.parseCommit(ObjectId.fromString(cId));
			RevTree tree = commit.getTree();
			try (TreeWalk treeWalk = new TreeWalk(repo)) {
				treeWalk.addTree(tree);
				treeWalk.setRecursive(true);
				while (treeWalk.next()) {
					if (treeWalk.isSubtree()) {
						treeWalk.enterSubtree();
					} else {
						FileDiff fileDiff = new FileDiff(Type.ADD, "/dev/null", treeWalk.getPathString());
						StringBuffer sBuffer = readFileContentFromCommit(repo, cId, treeWalk.getPathString());
						setModifiedRowsCount(fileDiff, sBuffer, true);
						files.add(fileDiff);
					}
				}
			}
		}
		return files;
	}

	/**
	 * Get target file content in certain commit.
	 * 
	 * @param repo
	 *            repository full name.
	 * @param cId
	 *            target commit hash id.
	 * @param file
	 *            certain file related path.
	 * @return the content of certain.
	 * @throws IOException
	 */
	public static StringBuffer readFileContentFromCommit(String repo, String cId, String file) throws IOException {
		StringBuffer content;
		try (Repository repository = CooperationHelper.openExistingRepo(repo)) {
			content = readFileContentFromCommit(repository, cId, file);
		}
		return content;
	}

	/**
	 * Get target file content in certain commit.
	 * 
	 * @param repo
	 *            repository instance.
	 * @param cId
	 *            target commit hash id.
	 * @param file
	 *            certain file related path.
	 * @return the content of certain.
	 * @throws IOException
	 */
	public static StringBuffer readFileContentFromCommit(Repository repo, String cId, String file)
			throws RevisionSyntaxException, AmbiguousObjectException, IncorrectObjectTypeException, IOException {
		StringBuffer sBuffer = new StringBuffer();
		try (RevWalk revWalk = new RevWalk(repo)) {
			RevCommit revCommit = revWalk.parseCommit(ObjectId.fromString(cId));
			RevTree tree = revCommit.getTree();
			try (TreeWalk treeWalk = new TreeWalk(repo); OutputStream outputStream = new ByteArrayOutputStream()) {
				treeWalk.addTree(tree);
				treeWalk.setRecursive(true);
				treeWalk.setFilter(PathFilter.create(file));
				if (!treeWalk.next()) {
					throw new IllegalStateException("Did not find expected file " + file);
				}
				ObjectLoader loader;
				ObjectId objectId;
				objectId = treeWalk.getObjectId(0);
				loader = repo.open(objectId);
				loader.copyTo(outputStream);
				sBuffer.append(outputStream.toString());
			}
		}
		return sBuffer;
	}

	private static void setModifiedRowsCount(FileDiff fileDiff, StringBuffer content, boolean isOrigin) {
		if (isOrigin) {
			int added = 0;
			int len = content.length();
			for (int i = 0; i < len; i++) {
				if (content.charAt(i) == '\n')
					added++;
			}
			fileDiff.setAdded(added);
			fileDiff.setRemoved(0);
			return;
		}
		int added = 0;
		int removed = 0;
		int len = content.length();
		boolean flag = false;
		for (int i = 0; i < len; i++) {
			switch (content.charAt(i)) {
			case '+':
				if (flag) {
					added++;
					flag = false;
				}
				break;
			case '-':
				if (flag) {
					removed++;
					flag = false;
				}
				break;
			case '\n':
				flag = true;
				break;
			default:
				if (flag)
					flag = false;
				break;
			}
		}
		added = added - 1 < 0 ? 0 : added - 1;
		removed = removed - 1 < 0 ? 0 : removed - 1;
		fileDiff.setAdded(added);
		fileDiff.setRemoved(removed);
	}
}
