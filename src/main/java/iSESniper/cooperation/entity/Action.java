package iSESniper.cooperation.entity;

import java.io.Serializable;
import java.util.TimeZone;

import iSESniper.cooperation.commit.enumeration.Type;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.cooperation.entity.Action.java
 *  
 *  2018年4月25日	下午1:22:45  
*/

public class Action implements Serializable {

	private static final long serialVersionUID = 5487246909735572090L;

	private Author author;
	
	private long addedLines;
	
	private long removedLines;
	
	private long time;
	
	private Type changeType;
	
	private TimeZone timeZone;

	public TimeZone getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	public Type getChangeType() {
		return changeType;
	}

	public void setChangeType(Type changeType) {
		this.changeType = changeType;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public long getAddedLines() {
		return addedLines;
	}

	public void setAddedLines(long addedLines) {
		this.addedLines = addedLines;
	}

	public long getRemovedLines() {
		return removedLines;
	}

	public void setRemovedLines(long removedLines) {
		this.removedLines = removedLines;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public Action(Author author, long addedLines, long removedLines, long time, Type changeType, TimeZone timeZone) {
		super();
		this.author = author;
		this.addedLines = addedLines;
		this.removedLines = removedLines;
		this.time = time;
		this.changeType = changeType;
		this.timeZone = timeZone;
	}

	public Action() {
		super(); 
	}

	@Override
	public String toString() {
		return "Action [author=" + author + ", addedLines=" + addedLines + ", removedLines=" + removedLines + ", time="
				+ time + ", changeType=" + changeType + "]";
	}

}
