package iSESniper.cooperation.entity;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.cooperation.entity.Author.java
 *  
 *  2018年2月28日	下午11:21:09  
*/

public class Author {
	
	private String name;
	
	private String email;

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Author(String name, String email) {
		super();
		this.name = name;
		this.email = email;
	}

	public Author() {
		super();
	}

	@Override
	public String toString() {
		return "Name: " + name + ", Email: " + email + "";
	}
	
	@Override
	public int hashCode() {
		return email.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		Author author = (Author)obj;
		return author.getEmail().equals(email);
	}
}
