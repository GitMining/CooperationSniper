package iSESniper.cooperation.entity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import iSESniper.cooperation.commit.enumeration.Type;

/**
 * @author : Magister
 * @fileName : iSESniper.cooperation.entity.Commit.java
 * 
 *           2018年2月24日 下午11:01:04
 */

public class Commit {

	private String commitId;

	private List<FileDiff> files;

	private Author author;

	private long when;

	private TimeZone timeZone;

	private String message;
	
	private List<Commit> parents;

	public List<Commit> getParents() {
		return parents;
	}

	public void setParents(List<Commit> parents) {
		this.parents = parents;
	}

	public void setCommitId(String commitId) {
		this.commitId = commitId;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public void setWhen(long when) {
		this.when = when;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public TimeZone getTimeZone() {
		return timeZone;
	}

	public String getCommitId() {
		return commitId;
	}

	public Author getAuthor() {
		return author;
	}

	public long getWhen() {
		return when;
	}

	public String getMessage() {
		return message;
	}
	
	public List<FileDiff> getFiles() {
		return files;
	}

	public void setFiles(List<FileDiff> files) {
		this.files = files;
	}

	public Commit(String commitId, List<FileDiff> files, Author author, long when, TimeZone timeZone, String message,
			List<Commit> parents) {
		super();
		this.commitId = commitId;
		this.files = files;
		this.author = author;
		this.when = when;
		this.timeZone = timeZone;
		this.message = message;
		this.parents = parents;
	}

	public Commit(String commitId, String author, String authorEmail, long when, TimeZone timeZone, String message) {
		super();
		this.commitId = commitId;
		// this.author = author;
		// this.authorEmail = authorEmail;
		this.author = new Author(author, authorEmail);
		this.when = when;
		this.timeZone = timeZone;
		this.message = message;
	}

	public Commit(String commitId, String author, String authorEmail, long when, TimeZone timeZone) {
		super();
		this.commitId = commitId;
		// this.author = author;
		// this.authorEmail = authorEmail;
		this.author = new Author(author, authorEmail);
		this.when = when;
		this.timeZone = timeZone;
	}

	public Commit(String commitId, Author author, long when, TimeZone timeZone) {
		this.commitId = commitId;
		this.author = author;
		this.when = when;
		this.timeZone = timeZone;
	}

	public Commit(String commitId, List<FileDiff> files, Author author, long when, TimeZone timeZone, String message) {
		super();
		this.commitId = commitId;
		this.files = files;
		this.author = author;
		this.when = when;
		this.timeZone = timeZone;
		this.message = message;
	}
	
	public Commit() {
		super();
	}

	public long[] countAddedAndRemovedLines() {
		long addedCount = 0;
		long removedCount=0;
		long[] count = new long[2];
		for(FileDiff fd: files) {
			addedCount += fd.getAdded();
			removedCount+=fd.getRemoved();
		}
		count[0] = addedCount;
		count[1] = removedCount;
		return count;
	}

	public Map<Type, List<FileDiff>> countFilesByChangeType() {
		Map<Type, List<FileDiff>> result = new HashMap<Type, List<FileDiff>>();
		if (files == null)
			return null;
		List<FileDiff> adds = new ArrayList<FileDiff>();
		List<FileDiff> dels = new ArrayList<FileDiff>();
		List<FileDiff> mdfs = new ArrayList<FileDiff>();
		List<FileDiff> rnms = new ArrayList<FileDiff>();
		List<FileDiff> cpys = new ArrayList<FileDiff>();
		for (FileDiff f : files) {
			switch (f.getChangeType()) {
			case ADD:
				adds.add(f);
				break;
			case DELETE:
				dels.add(f);
				break;
			case MODIFY:
				mdfs.add(f);
				break;
			case RENAME:
				rnms.add(f);
				break;
			case COPY:
				cpys.add(f);
				break;
			default:
				break;
			}
		}
		result.put(Type.ADD, adds);
		result.put(Type.DELETE, dels);
		result.put(Type.MODIFY, mdfs);
		result.put(Type.RENAME, rnms);
		result.put(Type.COPY, cpys);
		return result;
	}

	@Override
	public String toString() {
		final SimpleDateFormat dtfmt;
		dtfmt = new SimpleDateFormat("EEE MMM d HH:mm:ss yyyy Z", Locale.US);
		dtfmt.setTimeZone(timeZone);
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[Commit] ");
		sBuffer.append(commitId);
		sBuffer.append(", Author: ");
		sBuffer.append(author);
		sBuffer.append(", Time: ");
		sBuffer.append(dtfmt.format(Long.valueOf(when * 1000)));
		sBuffer.append(", Files: \n");
		if(files != null) {
			for(FileDiff fd: files) {
				sBuffer.append(fd.toString());
				sBuffer.append("\n");
			}	
		}
		return sBuffer.toString();
	}

}
