package iSESniper.cooperation.entity;

import java.util.Objects;

import iSESniper.cooperation.commit.enumeration.Type;

/**
 * @author : Magister
 * @fileName : iSESniper.cooperation.entity.File.java
 * 
 *           2018年3月10日 下午9:47:38
 */

public class FileDiff {

	private Type changeType;

	private String oldPth;

	private String newPth;

	private long added;

	private long removed;

	public FileDiff(Type changeType, String oldPth, String newPth, long added, long removed) {
		super();
		this.changeType = changeType;
		this.oldPth = oldPth;
		this.newPth = newPth;
		this.added = added;
		this.removed = removed;
	}

	public FileDiff(String oldPth, String newPth, Type changeType) {
		super();
		this.changeType = changeType;
		this.oldPth = oldPth;
		this.newPth = newPth;
	}

	public FileDiff(Type changeType, String oldPth, String newPth) {
		super();
		this.changeType = changeType;
		this.oldPth = oldPth;
		this.newPth = newPth;
	}

	public FileDiff() {
		super();
	}

	public Type getChangeType() {
		return changeType;
	}

	public String getOldPth() {
		return oldPth;
	}

	public String getNewPth() {
		return newPth;
	}

	public long getAdded() {
		return added;
	}

	public long getRemoved() {
		return removed;
	}

	public void setAdded(long added) {
		this.added = added;
	}

	public void setRemoved(long removed) {
		this.removed = removed;
	}

	public void setChangeType(Type changeType) {
		this.changeType = changeType;
	}

	public void setOldPth(String oldPth) {
		this.oldPth = oldPth;
	}

	public void setNewPth(String newPth) {
		this.newPth = newPth;
	}

	@Override
	public int hashCode() {
		return Objects.hash(newPth, oldPth, added, removed, changeType);
	}

	@Override
	public boolean equals(Object obj) {
		FileDiff object = (FileDiff) obj;
		if (this.newPth.equals(object.getNewPth()) && this.oldPth.equals(object.getOldPth())
				&& this.added == object.getAdded() && this.removed == object.getRemoved()
				&& this.changeType == object.getChangeType())
			return true;
		return false;
	}

	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[File] ChangeType: ");
		sBuffer.append(changeType);
		sBuffer.append(", OldPath: ");
		sBuffer.append(oldPth);
		sBuffer.append(", NewPath: ");
		sBuffer.append(newPth);
		sBuffer.append(" [added lines: ");
		sBuffer.append(added);
		sBuffer.append(" removed lines: ");
		sBuffer.append(removed);
		sBuffer.append("]");
		return sBuffer.toString();
	}

}
