package iSESniper.cooperation.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *  @author   :   Magister
 *  @fileName :   iSESniper.cooperation.entity.FileCommit.java
 *  @deprecated
 *  2018年2月24日	下午11:52:50  
*/

public class FileCommits {
	
	private String fileName;
	
	private List<Commit> commits;

	public String getFileName() {
		return fileName;
	}

	public List<Commit> getCommits() {
		return commits;
	}

	public FileCommits(String fileName, List<Commit> commits) {
		super();
		this.fileName = fileName;
		this.commits = commits;
	}

	public List<String> getAuthors(){
		List<String> authors = new ArrayList<String>();
//		Author a;
		for(Commit c: commits) {
			String name = c.getAuthor().toString();
//			String email = c.getAuthorEmail();
//			a = new Author(name, email);
			if(!authors.contains(name))authors.add(name);
		}
		return authors;
	}
	
	@Override
	public String toString() {
		StringBuilder r = new StringBuilder();
		r.append("[FileCommits] [fileName: " + fileName + ", commits: \n" );
		for(Commit c: commits) {
			r.append(c.toString() + "\n");
		}
		r.append("Authors:\n" + getAuthors().toString());
		r.append("]\n");
		return r.toString();
	}
	
	
}
