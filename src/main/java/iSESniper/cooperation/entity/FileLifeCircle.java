package iSESniper.cooperation.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * File's life circle includes a serials of actions that authors contributed.
 *  @author   :   Magister
 *  @fileName :   iSESniper.cooperation.entity.FileLifeCircle.java
 *  
 *  2018年4月25日	下午1:25:10  
*/

public class FileLifeCircle implements Serializable{

	private static final long serialVersionUID = -1573438076865254943L;
	
	private String fileName;
	
	private List<Action> actions;
	
	private Map<Author, Integer> proportion;

	public Map<Author, Integer> getProportion() {
		return proportion;
	}

	public void setProportion(Map<Author, Integer> proportion) {
		this.proportion = proportion;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<Action> getActions() {
		return actions;
	}

	public void setActions(List<Action> actions) {
		this.actions = actions;
	}

	public FileLifeCircle(String fileName, List<Action> actions, Map<Author, Integer> proportion) {
		super();
		this.fileName = fileName;
		this.actions = actions;
		this.proportion = proportion;
	}

	public FileLifeCircle() {
		super();
	}
	
	@Override
	public int hashCode() {
		return fileName.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		FileLifeCircle fileLifeCircle = (FileLifeCircle) obj;
		return fileName.equals(fileLifeCircle.getFileName());
	}

	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[FileLifeCircle] FileName: ");
		sBuffer.append(fileName);
		sBuffer.append(", actions: \n");
		if(actions != null || actions.size() != 0)
			for(Action a: actions) {
				sBuffer.append(a);
				sBuffer.append("\n");
			}
		else
			sBuffer.append("Action List is Empty.");
		return sBuffer.toString();
	}
}
